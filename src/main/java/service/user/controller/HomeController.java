package service.user.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import service.user.entity.UserDetailRedis;
import service.user.entity.UserContact;
import service.user.entity.Users;
import service.user.repository.UserContactRepository;
import service.user.repository.UserDetailRepository;
import service.user.repository.UserRerpository;
import service.user.repository.UserDetailRedisRepository;
import service.user.request.UserRequest;
import service.user.request.UserSearchRequest;
import service.user.service.ProducerService;


@RestController
public class HomeController {
	
	private static final Logger LOGGER = LogManager.getLogger(HomeController.class);
	
	@Autowired
	Environment env;
	
	@Autowired
	UserDetailRepository userDetailRepository;
	
	@Autowired
	UserRerpository userRepository;
	
	@Autowired
	UserContactRepository userContactRepository;
	
	@Autowired
	UserDetailRedisRepository userDetailRedisRepository;
	
	@Autowired
	ProducerService producerService;
	
	@ResponseBody
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String getHome() {
		LOGGER.info("masuk home");
		return "sucess";
	}
	
	@ResponseBody
	@RequestMapping(value = "/users/all", method = RequestMethod.POST)
	public ResponseEntity<?> getUserAllDetail() throws Exception {
        try {
        	List list = userDetailRepository.listUserDetail();
        	Map result = new HashMap();
        	result.put("data", list);
        	return ResponseEntity.ok(result);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("ERROR " + e.getMessage());
            throw e;
        }
    }
	
	@ResponseBody
	@RequestMapping(value = "/users/byName", method = RequestMethod.POST)
	public ResponseEntity<?> getUserDetailByName(@Validated @RequestBody UserSearchRequest userRequest) throws Exception {
        try {
        	String name = userRequest.getName().concat("%");
        	List list = userDetailRepository.listUserDetailByName(name);
        	
        	Map result = new HashMap();
        	result.put("data", list);
        	return ResponseEntity.ok(result);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("ERROR " + e.getMessage());
            throw e;
        }
    }

	@ResponseBody
	@RequestMapping(value = "/users/byAddress", method = RequestMethod.POST)
	public ResponseEntity<?> getUserDetailByAddress(@Validated @RequestBody UserSearchRequest userRequest) throws Exception {
        try {
        	String address = userRequest.getAddress().concat("%");
        	List list = userDetailRepository.listUserDetailByAddress(address);
        	
        	Map result = new HashMap();
        	result.put("data", list);
        	return ResponseEntity.ok(result);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("ERROR " + e.getMessage());
            throw e;
        }
    }
	
	@ResponseBody
	@RequestMapping(value = "/users/age/equals", method = RequestMethod.POST)
	public ResponseEntity<?> getUserDetailByAgeEquals(@Validated @RequestBody UserSearchRequest userRequest) throws Exception {
        try {
        	List list = userDetailRepository.listUserDetailByAge(userRequest.getAge());
        	
        	Map result = new HashMap();
        	result.put("data", list);
        	return ResponseEntity.ok(result);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("ERROR " + e.getMessage());
            throw e;
        }
    }
	
	@ResponseBody
	@RequestMapping(value = "/users/age/younger", method = RequestMethod.POST)
	public ResponseEntity<?> getUserDetailByYounger(@Validated @RequestBody UserSearchRequest userRequest) throws Exception {
        try {
        	List list = userDetailRepository.listUserDetailByYounger(userRequest.getAge());
        	
        	Map result = new HashMap();
        	result.put("data", list);
        	return ResponseEntity.ok(result);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("ERROR " + e.getMessage());
            throw e;
        }
    }
	
	@ResponseBody
	@RequestMapping(value = "/users/age/older", method = RequestMethod.POST)
	public ResponseEntity<?> getUserDetailByOlder(@Validated @RequestBody UserSearchRequest userRequest) throws Exception {
        try {
        	List list = userDetailRepository.listUserDetailByOlder(userRequest.getAge());
        	
        	Map result = new HashMap();
        	result.put("data", list);
        	return ResponseEntity.ok(result);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("ERROR " + e.getMessage());
            throw e;
        }
    }
	
	@ResponseBody
	@RequestMapping(value = "/user/put", method = RequestMethod.PUT)
	public ResponseEntity<?> putUser(@Validated @RequestBody UserRequest userRequest) throws Exception {
        try {
            
            Users user = new Users();
            user.setId(RandomStringUtils.randomAlphanumeric(15));
            user.setName(userRequest.getName());
            user.setAge(userRequest.getAge());
            
            userRepository.save(user);

            UserContact userContact = new UserContact();
            userContact.setId(RandomStringUtils.randomAlphanumeric(15));
            userContact.setAddress(userRequest.getAddress());
            userContact.setUserId(user.getId());
            
            
            userContactRepository.save(userContact);
        	return ResponseEntity.ok("success");
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.info("ERROR " + e.getMessage());
            throw e;
        }
    }
	
	@ResponseBody
    @Cacheable(value = "user_detail_redis")
    @RequestMapping(value = "/user/cache/get/all", method = RequestMethod.GET)
    public List<UserDetailRedis> getUserDetailRedisAll() {
        return userDetailRedisRepository.findAll();
    }

	@ResponseBody
    @Cacheable(value = "user_detail_redis", key = "#id")
    @RequestMapping(value = "/user/cache/get/{id}", method = RequestMethod.GET)
    public UserDetailRedis getUserDetail(@PathVariable String id) {
       
        Optional<UserDetailRedis> userDetail = userDetailRedisRepository.findById(id);
        if(!userDetail.isPresent()) {
        	throw new RuntimeException("Record not found");
        }
        return userDetail.get();
    }

    @ResponseBody
    @CachePut(value = "user_detail_redis")
    @RequestMapping(value = "/user/cache/create", method = RequestMethod.PUT)
    public UserDetailRedis createUserCache(@RequestBody UserDetailRedis userDetail) {
        userDetailRedisRepository.save(userDetail);
        return userDetail;
    }
    
    @ResponseBody
    @CachePut(value = "user_detail_redis", key = "#user_detail_redis.id")
	@RequestMapping(value = "/user/cache/update", method = RequestMethod.PUT)
	public UserDetailRedis putUserCache(@Validated @RequestBody UserDetailRedis userRequest) throws Exception {
    	userDetailRedisRepository.save(userRequest);
		return userRequest;
	}

	@ResponseBody
	@RequestMapping(value = "/user/cache/delete/{id}", method = RequestMethod.DELETE)
	public String deleteStudent(@PathVariable String id) {
		
        producerService.sendMessage(id);
        
		return "Record deleted succesfully";
	}
}
