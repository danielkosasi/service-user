package service.user.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import service.user.entity.UserDetailRedis;
import service.user.repository.UserDetailRedisRepository;

@Service
public final class ConsumerService {
	
	@Autowired
	UserDetailRedisRepository userDetailRedisRepository;

	@KafkaListener(topics = "deleteUserDetail", groupId = "group_id")
	public void consume(String id) {
		Optional<UserDetailRedis> userDetail = userDetailRedisRepository.findById(id);
        if(!userDetail.isPresent()) {
        	throw new RuntimeException("Record not found");
        }else {
        	userDetailRedisRepository.delete(userDetail.get());
        }
        
	}
}