package service.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import service.user.entity.Users;


@Repository
public interface UserRerpository extends JpaRepository<Users, String> {
}
