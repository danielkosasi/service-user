package service.user.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import service.user.entity.UserDetail;

@Repository
public interface UserDetailRepository extends CrudRepository<UserDetail, String> {
	
	@Query(value = "select us.id, us.name, us.age, uc.address from users us INNER JOIN user_contact uc on us.id = uc.user_id", nativeQuery = true)
	List<UserDetail> listUserDetail();
	
	@Query(value = "select us.id, us.name, us.age, uc.address from users us INNER JOIN user_contact uc on us.id = uc.user_id where us.name like ?1", nativeQuery = true)
	List<UserDetail> listUserDetailByName(String name);
	
	@Query(value = "select us.id, us.name, us.age, uc.address from users us INNER JOIN user_contact uc on us.id = uc.user_id where uc.address like :address", nativeQuery = true)
	List<UserDetail> listUserDetailByAddress(@Param("address") String address);
	
	@Query(value = "select us.id, us.name, us.age, uc.address from users us INNER JOIN user_contact uc on us.id = uc.user_id where us.age = ?1", nativeQuery = true)
	List<UserDetail> listUserDetailByAge(int age);

	@Query(value = "select us.id, us.name, us.age, uc.address from users us INNER JOIN user_contact uc on us.id = uc.user_id where us.age < ?1", nativeQuery = true)
	List<UserDetail> listUserDetailByYounger(int age);
	
	@Query(value = "select us.id, us.name, us.age, uc.address from users us INNER JOIN user_contact uc on us.id = uc.user_id where us.age > ?1", nativeQuery = true)
	List<UserDetail> listUserDetailByOlder(int age);

}
