package service.user.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import service.user.entity.UserContact;

@Repository
public interface UserContactRepository extends CrudRepository<UserContact, String> {

}
