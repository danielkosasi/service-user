package service.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import service.user.entity.UserDetailRedis;

@Repository
public interface UserDetailRedisRepository extends JpaRepository<UserDetailRedis, String> {

}
