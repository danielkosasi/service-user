package service.user.entity;

import javax.persistence.*;

import org.apache.commons.lang.RandomStringUtils;

@Entity
@Table(name = "user_contact")
public class UserContact {

	@Id
	@Column(name = "id", unique = true, nullable = false)
	private String id;

	@Column(name = "address")
	private String address;
	
	@Column(name = "user_id")
	private String userId;
	
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


	public UserContact() {
		
	}
	
	public UserContact(String address) {
		this.id = RandomStringUtils.random(15);
		this.address = address;
	}
}

