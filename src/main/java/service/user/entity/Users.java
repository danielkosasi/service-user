package service.user.entity;

import javax.persistence.*;

import org.apache.commons.lang.RandomStringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "users")
public class Users {

	@Id
	@Column(name = "id", unique = true, nullable = false)
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "age")
	private int age;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", age=" + age + "]";
	}
	
	public Users() {
		
	}
	
	public Users(String name, int age) {
		this.id = RandomStringUtils.random(15);
		this.name = name;
		this.age = age;
	}
}

